// ==UserScript==
// @name         Jira-Harvest resetter
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Reset project selection in Jira-Harvest plugin and try to find and pre-select project by Epic Link/Name
// @author       pilat@x-cart.com
// @match        https://jira-harvest.42nd.co/*
// @icon         https://www.google.com/s2/favicons?domain=atlassian.net
// @run-at       document-idle
// @grant        none
// ==/UserScript==

async function jiraHarvestMain() {
    'use strict';

    function resetProjectAndTask(jiraName = null) {
        localStorage['undefined/project'] = '';
        localStorage['undefined/task'] = localStorage.last_selected_task_id ?? '';

        if (jiraName) {
            localStorage[jiraName + '/project'] = '';
            localStorage[jiraName + '/task'] = localStorage.last_selected_task_id ?? '';
        }
    }

    function handleTaskChange(e) {
        const withinTaskSelector = e.target.closest('[data-test="task"]');

        if (withinTaskSelector && e.target.value) {
            localStorage.last_selected_task_id = e.target.value;
        }
    }

    async function getJiraData(issueId) {
        var response = await AP.request(`/rest/agile/1.0/issue/${issueId}?fields=summary,epic`);

        var data = JSON.parse(response.body)

        const jiraName = data.fields.summary;
        const jiraEpic = data.fields.epic ? data.fields.epic.name : '';

        return {
            jiraName,
            jiraEpic
        };
    }

    function getStylesheet() {
        let linkEl = document.getElementById('jira_harvest_element');
        if (!linkEl) {
            linkEl = document.createElement("style");
            linkEl.id = 'jira_harvest_element';

            document.head.appendChild(linkEl);
        }

        // clean up
        for (let idx in linkEl.sheet.cssRules) {
            if (idx == parseInt(idx)) { linkEl.sheet.deleteRule(idx); }
        }

        return linkEl.sheet;
    }
    function markProjectFound() {
        getStylesheet().insertRule('[data-test="project"] .ss-single-selected { background: #dfffdf; }');
    }
    function markProjectNotFound() {
        getStylesheet().insertRule('[data-test="project"] .ss-single-selected { background: #ffeccf; }');
    }


    // Bind handler to save last-selected task:
    document.querySelector('body').addEventListener('change', handleTaskChange);

    // Reset Project and Task with empty values
    resetProjectAndTask();

    // Get data about Jira ticket
    const dataFromParent = JSON.parse(window.name);
    const { jiraName, jiraEpic } = await getJiraData(dataFromParent.options.structuredContext.jira.issue.id);

    // Reset again, but with Jire Name this tike (implementation nuances…)
    resetProjectAndTask(jiraName);

    // Mark Project selector as "Nort found"
    markProjectNotFound();

    // … trying to select a project by Epic Name from Jira:
    if (jiraEpic) {
        JSON.parse(localStorage.projects).value.forEach(harvestClient => {
            harvestClient.options.forEach(harvestProject => {
                if (harvestProject.name.indexOf(jiraEpic) !== -1) {
                    // Select matching Project
                    localStorage['undefined/project'] = harvestProject.value;
                    localStorage[jiraName + '/project'] = harvestProject.value;

                    // … and mark Project selector as "Found"
                    markProjectFound();
                }
            });
        });
    }
}

jiraHarvestMain();
